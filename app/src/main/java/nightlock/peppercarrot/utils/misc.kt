/*
 * Copyright (C) 2017 - 2019 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Pepper&Carrot e-reader.
 *
 * Pepper&Carrot e-reader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package nightlock.peppercarrot.utils

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import androidx.work.Constraints
import androidx.work.NetworkType
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import com.google.gson.GsonBuilder
import nightlock.peppercarrot.ComicUpdateWorker
import java.util.concurrent.TimeUnit

/**
 * Miscellaneous utility functions.
 * Created by Jihoon Kim on 21/07/17.
 */

const val ROOT_URL = "https://www.peppercarrot.com/0_sources/"

fun getCoverImageUrl(episode: Episode, language: Language) =
        ROOT_URL + "${episode.name}/low-res/${language.pncCode}_Pepper-and-Carrot_by-David-Revoy_" + "E${String.format(
                "%1$02d", episode.index + 1
        )}.jpg"

fun getEmptyCoverImageUrl(episode: Episode) =
        ROOT_URL + "${episode.name}/low-res/gfx-only/gfx_Pepper-and-Carrot_by-David-Revoy_" + "E${String.format(
                "%1$02d", episode.index + 1
        )}.jpg"

fun getBaseComicImageUrl(episode: Episode, language: Language, index: Int) =
        ROOT_URL + "${episode.name}/low-res/${language.pncCode}_Pepper-and-Carrot_by-David-Revoy_" + "E${String.format(
                "%1$02d", episode.index + 1
        )}P${String.format("%1$02d", index)}"

fun listToJson(list: List<String>): String = GsonBuilder().create().toJson(list)

fun jsonToList(string: String): MutableList<String>? =
        GsonBuilder().create().fromJson(string, Array<String>::class.java).toMutableList()

fun refreshWork(context: Context) = WorkManager.getInstance(context).run {
    cancelAllWorkByTag(ComicUpdateWorker.WORK_TAG)

    enqueue(PeriodicWorkRequestBuilder<ComicUpdateWorker>(1, TimeUnit.HOURS)
            .setConstraints(Constraints
                    .Builder()
                    .setRequiredNetworkType(if (PreferenceManager
                                    .getDefaultSharedPreferences(context)
                                    .getBoolean(Constants.MOBILE_AVAILABLE_PREF, false)
                    ) NetworkType.CONNECTED else NetworkType.UNMETERED
                    )
                    .setRequiresBatteryNotLow(true)
                    .build()
            )
            .addTag(ComicUpdateWorker.WORK_TAG)
            .build()
    )
}

fun dpFromPx(context: Context, px: Float): Float = px / context.resources.displayMetrics.density

fun getSafePrefString(preferences: SharedPreferences, key: String, default: String) =
        preferences.getString(key, default) ?: default