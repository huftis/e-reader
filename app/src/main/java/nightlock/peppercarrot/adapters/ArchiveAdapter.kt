/*
 * Copyright (C) 2017 - 2019 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Pepper&Carrot e-reader.
 *
 * Pepper&Carrot e-reader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nightlock.peppercarrot.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import nightlock.peppercarrot.utils.Episode
import nightlock.peppercarrot.utils.Language
import nightlock.peppercarrot.viewholders.EpisodeCoverViewHolder

/**
 * Adapter for Displaying EpisodeCoverViewHolder
 * Created by Jihoon Kim on 5/2/17.
 */

class ArchiveAdapter(val language: Language) : RecyclerView.Adapter<EpisodeCoverViewHolder>() {
    private val episodeList = ArrayList<Episode>()

    override fun getItemCount() = episodeList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            EpisodeCoverViewHolder.newInstance(parent, language)

    override fun onBindViewHolder(holder: EpisodeCoverViewHolder, position: Int) =
            holder.onBind(episodeList[position])

    fun addAndNotify(episode: Episode) {
        val isAdded = episodeList.add(episode)
        if (isAdded) notifyItemInserted(itemCount - 1)
    }

    fun sortList(comparator: Comparator<in Episode>) = episodeList.sortWith(comparator)
}
