/*
 * Copyright (C) 2017 - 2019 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Pepper&Carrot e-reader.
 *
 * Pepper&Carrot e-reader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nightlock.peppercarrot.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import nightlock.peppercarrot.fragments.ComicViewerFragment
import nightlock.peppercarrot.utils.Episode
import nightlock.peppercarrot.utils.Language
import nightlock.peppercarrot.utils.getBaseComicImageUrl

/**
 * Adapter for Displaying ComicViewHolder
 * Created by Jihoon Kim on 5/7/17.
 */

class ComicViewerAdapter(val episode: Episode, val language: Language, fm: FragmentManager) :
        FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment =
            ComicViewerFragment.newInstance(getBaseComicImageUrl(episode, language, position))

    override fun getCount() = episode.pages
}
